import requests
import magic
import os
import urllib
from urllib.parse import urlparse

VALID_IMAGE_MIMETYPES = ['image']

class UrlHeaderParser:

    def __init__(self, url):
        self.url = url

    def get_content_type(self):
        r = requests.get(self.url, stream=True)
        try:
            peek = r.iter_content(256).next()
        except AttributeError:
            peek = next(r.iter_content(256))
        mime = magic.from_buffer(peek, mime=True)
        return mime

    def content_is_image(self):
        mime = self.get_content_type()
        if mime.split('/')[0] == 'image':
            return True
        return False



class UrlParser:

    def __init__(self, url):
        self.url = url
        self.result = urllib.request.urlretrieve(url)
        self.header_parser = UrlHeaderParser(url)

    def get_filename(self):
        result = urlparse(self.url)
        filename = os.path.basename(result.path)
        if filename[0] == '/':
            filename = filename[1:]
        if '.' in filename:
            filename = filename.split('.')[0]

        return filename

    def get_content(self):
        return self.result[0]

    def get_extension(self):
         content_type = self.header_parser.get_content_type()
         return content_type.split('/')[1]