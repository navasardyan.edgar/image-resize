from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls.static import static
from idagroup import settings
from upload.views import resize

handler404 = 'idagroup.views.asdasds'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('upload.urls')),
    re_path(r'^resize/(?P<format>[,\w-]+)/(?P<url>.*)/?$', resize, name='resize'),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)