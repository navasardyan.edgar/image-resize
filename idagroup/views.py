from django.shortcuts import render_to_response
from django.template import RequestContext


def asdasds(request, exception):
    context = RequestContext(request)
    response = render_to_response('404.html', context)
    response.status_code = 404
    return response
