import os
from idagroup.settings import EXT_TO_FORMAT, EXT_TO_FORMAT_DEFAULT


def ext_to_format(filename):
    extension = os.path.splitext(filename)[1].lower()
    format = EXT_TO_FORMAT.get(extension, EXT_TO_FORMAT_DEFAULT)
    if not format:
        raise KeyError('Unknown image extension: {0}'.format(extension))
    return format

def get_image_size_in_bytes(image):
    size = os.stat(image.path).st_size
    return size
