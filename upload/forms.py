from django import forms
from django.core.exceptions import ValidationError
from django.core.files import File as django_core_file
from PIL import Image
from upload.models import File
from urltools.parsers import UrlHeaderParser


VALID_IMAGE_MIMETYPES = [
    "image"
]

class FileUploadForm(forms.ModelForm):

    class Meta:
        model = File
        fields = ['url', 'image']


    def clean_url(self):
        url = self.cleaned_data.get('url')

        if url:
            url_header_parser = UrlHeaderParser(url)
            if not url_header_parser.content_is_image():
                raise forms.ValidationError("This url does not seem to contain an image!")

        return url

    def clean(self):
        cleaned_data = super().clean()
        image = cleaned_data.get("image")
        url = cleaned_data.get("url")

        if image and url:
            raise ValidationError(
                "Please fill in EITHER file OR url, not both"
            )

        if image is None and url is None:
            raise ValidationError(
                "Please fill in url or file"
            )

