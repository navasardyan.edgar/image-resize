from django.core.cache import cache
from django.views.generic import ListView, CreateView, DetailView

import os
import stat
import time

from django.http import HttpResponse, HttpResponseNotModified

from django.utils.http import http_date
from django.views.static import was_modified_since


from idagroup import settings

from upload.models import File, Image
from upload.forms import FileUploadForm


class HomePageView(ListView):
    model = File
    template_name = 'homepage/index.html'


class FileDetailsView(DetailView):
    template_name = 'file_details.html'
    model = File

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        get_params = self.request.GET
        if len(get_params) == 0:
            return data
        width = get_params.get('width', '')
        height = get_params.get('height', '')
        size = get_params.get('size', '')
        data['resize_params'] = 'x'.join([str(width), str(height), str(size)])
        return data


class UploadFileView(CreateView):
    template_name = 'upload_file.html'
    form_class = FileUploadForm

    def get_success_url(self):
        return '/'

def _image_response(image):
    response = HttpResponse(
        image.render(),
        image.mimetype
    )
    response['Last-Modified'] = http_date(image.modified)
    expire_time = getattr(settings, 'IMAGEFIT_EXPIRE_HEADER', 3600*24*30)
    response['Expires'] = http_date(time.time() + expire_time)
    return response


def resize(request, format, url):
    width, height, size = format.split('x')

    prefix = settings.MEDIA_ROOT
    image = Image(path=os.path.join(prefix, url))
    if not os.path.exists(image.path):
        return HttpResponse(status=404)

    stat_obj = os.stat(image.path)
    if not was_modified_since(request.META.get('HTTP_IF_MODIFIED_SINCE'),
                              stat_obj[stat.ST_MTIME], stat_obj[stat.ST_SIZE]):
        return HttpResponseNotModified(content_type=image.mimetype)

    image.cache = cache
    image.cached_name = request.META.get('PATH_INFO')

    if image.is_cached:
        return _image_response(image)

    image.resize(width=width, height=height)
    if size:
        image.save(max_bytes=size)
    else:
        image.save()

    return _image_response(image)
