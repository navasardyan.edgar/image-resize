from django import template
from django.urls import reverse
register = template.Library()


@register.filter
def media_resize(url, format):

    return reverse('resize', kwargs=dict(
        url=url, format=format))