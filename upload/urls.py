from django.urls import path, re_path, include
from upload import views

app_name = 'upload'
urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('upload/', views.UploadFileView.as_view(), name='file-create'),
    path('file/<int:pk>', views.FileDetailsView.as_view(), name='file-details'),

]