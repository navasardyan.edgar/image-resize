from __future__ import division
from django.db import models
from django.urls import reverse
from io import BytesIO
import logging
import math
import mimetypes
import os
from PIL import Image as PilImage
from upload.utils import ext_to_format
from django.core.files import File as django_core_file
from urltools.parsers import UrlParser


logger = logging.getLogger(__name__)


class File(models.Model):
    url = models.URLField(max_length=1024, null=True, blank=True)
    image = models.ImageField(null=True, blank=True)

    class Meta:
        db_table = 'file'

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        kwargs = {"pk": self.id}
        return reverse("upload:file-details", kwargs=kwargs)

    def save(self, *args, **kwargs):
        if self.url and not self.image:

            url_parser = UrlParser(self.url)

            filename = url_parser.get_filename()
            extension = url_parser.get_extension()
            content = url_parser.get_content()

            with open(content, 'rb') as f:
                self.image.save(
                    '{}.{}'.format(filename, extension),
                    django_core_file(f))
        else:
            super().save(*args, **kwargs)


class Image:

    class resize_error(Exception): pass

    def __init__(self, path, cache=None, cached_name=None, *args, **kwargs):
        self.path = path

        self.pil = PilImage.open(path)
        self.cache = cache
        self.cached_name = cached_name
        self.quality = None

        if self.pil.mode not in ('L', 'RGB', 'LA', 'RGBA'):
            self.pil = self.pil.convert('RGB')

    @property
    def mimetype(self):
        return mimetypes.guess_type(self.path)[0]

    @property
    def modified(self):
        return os.path.getmtime(self.path)

    @property
    def is_cached(self):
        return self.cache and self.cached_name in self.cache

    def resize(self, width='', height='', preserve_aspect_ration=False):
        if width == '' and height == '':
            return self.pil

        if width == '':
            width = self.pil.width
        if height == '':
            height = self.pil.height

        size = [int(width), int(height)]
        if preserve_aspect_ration:
            self.pil.thumbnail(size, PilImage.ANTIALIAS)
        else:
            self.pil = self.pil.resize(size, PilImage.ANTIALIAS)

        return self.pil


    def get_highest_acceptable_quality(self, extension, max_bytes):
        if extension.lower() != 'jpeg':
            return None

        max_bytes = int(max_bytes)
        q_min, q_max = 1, 96
        q_acc = -1
        image = self.pil
        while q_min <= q_max:
            m = math.floor((q_min + q_max) / 2)
            buffer = BytesIO()
            image.save(buffer, format="jpeg", quality=m)
            s = buffer.getbuffer().nbytes

            if s <= max_bytes:
                q_acc = m
                q_min = m + 1
            elif s > max_bytes:
                q_max = m - 1

        if q_acc > -1:
            return q_acc
        else:
            logger.warning('No acceptable quality factor found')
            return None

    def render(self):
        if self.is_cached:
            return self.cache.get(self.cached_name)
        else:
            image_str = BytesIO()
            self.pil.save(image_str, self.get_extension(self.cached_name))
            return image_str.getvalue()

    def save(self, max_bytes = None):
        if self.cache and not self.is_cached:
            image_str = BytesIO()
            quality = None

            extension = ext_to_format(self.cached_name)

            if max_bytes is not None:
                quality = self.get_highest_acceptable_quality(extension, max_bytes)

            if quality:
                self.pil.save(image_str, extension, quality=quality)
            else:
                self.pil.save(image_str, extension)

            self.cache.set(self.cached_name, image_str.getvalue())
            image_str.close()

