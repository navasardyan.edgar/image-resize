import tempfile
import urllib

from django.test import TestCase
from django.urls import reverse
from django.test import override_settings

from upload.models import File, Image
from upload.tests.utils import get_temporary_image


class UploadFileViewTest(TestCase):
    
    def setUp(self):
        pass
        
    @classmethod
    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def setUpTestData(cls):
        temp_file = get_temporary_image()
        File.objects.create(image=temp_file.name)

    def test_files_list(self):
        response = self.client.get(reverse('upload:home'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context['file_list']) == 1)

    def test_file_detail(self):
        response = self.client.get(reverse('upload:file-details', args=[1]))
        self.assertEqual(response.status_code, 200)
        
    def test_file_detail_with_width(self):
        query_params = {'width': 100}
        response = self.client.get(reverse('upload:file-details', kwargs={
            'pk': 1}) + '?' + urllib.parse.urlencode(query_params))
        self.assertEqual(response.status_code, 200)
        
    def test_file_detail_with_height(self):
        query_params = {'height': 100}
        response = self.client.get(reverse('upload:file-details', kwargs={
            'pk': 1}) + '?' + urllib.parse.urlencode(query_params))
        self.assertEqual(response.status_code, 200)
        
    def test_file_detail_with_size(self):
        query_params = {'size': 100}
        response = self.client.get(reverse('upload:file-details', kwargs={
            'pk': 1}) + '?' + urllib.parse.urlencode(query_params))
        self.assertEqual(response.status_code, 200)
        
    def test_file_detail_with_width_and_height(self):
        query_params = {'size': 100, 'width': 100}
        response = self.client.get(reverse('upload:file-details', kwargs={
            'pk': 1}) + '?' + urllib.parse.urlencode(query_params))
        self.assertEqual(response.status_code, 200)
        
    def test_file_detail_with_all_params(self):
        query_params = {'size': 100, 'width': 100, 'height': 100}
        response = self.client.get(reverse('upload:file-details', kwargs={
            'pk': 1}) + '?' + urllib.parse.urlencode(query_params))
        self.assertEqual(response.status_code, 200)
        
        
     


