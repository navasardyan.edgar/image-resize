import tempfile
from PIL import Image


def get_temporary_image():
    temp_file = tempfile.NamedTemporaryFile()
    size = (200, 200)
    color = (255, 0, 0, 0)
    image = Image.new("RGB", size, color)
    image.save(temp_file, 'jpeg')
    return temp_file