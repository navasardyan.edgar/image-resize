import os
import tempfile

from django.test import TestCase
from shutil import rmtree
from django.test import override_settings

from idagroup import settings
from upload.models import File, Image
from upload.tests.utils import get_temporary_image
from upload.utils import get_image_size_in_bytes


class ImageModelTest(TestCase):

    @classmethod
    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def setUpTestData(cls):

        temp_file = get_temporary_image()
        cls.image = Image(path=temp_file.name)

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_resize_by_height_without_preserved_aspect_ratio(self):
        image = self.image
        initial_height = image.pil.height
        initial_width = image.pil.width
        target_height = initial_height - 10

        image.resize(height=target_height, preserve_aspect_ration=False)
        image.save()

        self.assertEqual(self.image.pil.width, initial_width)
        self.assertEqual(self.image.pil.height, target_height)

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_resize_by_width_without_preserved_aspect_ratio(self):
         image = self.image
         initial_height = image.pil.height
         initial_width = image.pil.width

         target_width = initial_width - 10

         image.resize(width=target_width, preserve_aspect_ration=False)
         image.save()

         self.assertEqual(self.image.pil.width, target_width)
         self.assertEqual(self.image.pil.height, initial_height)

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_resize_by_width_and_height_without_preserved_aspect_ratio(self):
         image = self.image
         initial_height = image.pil.height
         initial_width = image.pil.width

         target_width = initial_width - 10
         target_height = initial_height - 15

         image.resize(width=target_width, height=target_height, preserve_aspect_ration=False)
         image.save()

         self.assertEqual(image.pil.width, target_width)
         self.assertEqual(image.pil.height, target_height)

