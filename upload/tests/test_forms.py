import tempfile


from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.test import override_settings

from idagroup import settings
from upload.forms import FileUploadForm
from upload.tests.utils import get_temporary_image


class UploadFileFormTest(TestCase):
    
    def test_form_ensure_either_image_or_url(self):
        post_dict = {'url': None}
        file_dict = {'image': None}
        form = FileUploadForm(post_dict, file_dict)
        self.assertFalse(form.is_valid())

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_form_prohibit_both_image_and_url(self):
        temp_file = get_temporary_image()
        with open(temp_file.name, 'rb') as f:
            temp_image = SimpleUploadedFile(name=f.name,
                                            content=f.read(),
                                            content_type='image/jpeg')

        post_dict = {'url': 'google.com'}
        file_dict = {'image': temp_image}
        form = FileUploadForm(post_dict, file_dict)
        self.assertFalse(form.is_valid())
