import os
import tempfile

from django.test import TestCase
from shutil import rmtree
from django.test import override_settings

from idagroup import settings
from upload.models import File, Image
from upload.tests.utils import get_temporary_image
from upload.utils import get_image_size_in_bytes


class FileModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_create_instance_from_image(self):
        temp_image = get_temporary_image()
        file = File.objects.create(image=temp_image.name)
        self.assertEqual(file.id, 1)
        # self.assertEqual(file.image.width, 200)
        # self.assertEqual(file.image.height, 200)
        # self.assertEqual(file.image.size, 215579)

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_create_instance_from_url(self):
        url = "https://images.unsplash.com/photo-1472214103451-9374bd1c798e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
        file = File.objects.create(url=url)
        self.assertEqual(len(File.objects.all()), 1)
        self.assertEqual(file.image.width, 1000)
        self.assertEqual(file.image.height, 667)
        self.assertEqual(file.image.size, 215579)

    def test_get_absolute_url(self):
        self.test_create_instance_from_url()
        self.assertEquals(File.objects.all().first().get_absolute_url(), '/file/1')
