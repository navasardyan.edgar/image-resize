rm -rf env
virtualenv venv
. ./venv/bin/activate
pip install -r requirements.txt
rm db.sqlite3
python manage.py migrate
python manage.py makemigrations upload
python manage.py migrate
